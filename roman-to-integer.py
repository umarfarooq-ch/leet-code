# created by Muhammad Umar Farooq
# date: 09.10.2022

# link: https://leetcode.com/problems/roman-to-integer/


class Solution:

    def romanToInt(self, s: str) -> int:
        equivalents = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000,
        }

        str_len = len(s)
        number = 0
        for index in range(str_len):
            if index == str_len - 1 or equivalents[s[index]] >= equivalents[s[index + 1]]:
                number += equivalents[s[index]]

            else:
                number -= equivalents[s[index]]

        return number


def main():
    s = Solution()
    print(s.romanToInt(s='MCMXCIV'))
    # same


if __name__ == '__main__':
    main()