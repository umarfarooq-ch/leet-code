# created by Muhammad Umar Farooq
# date: 15.10.2022

# link: https://leetcode.com/problems/add-two-numbers/

from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def r(l1, l2, carry):
    if l1 is None and l2 is None:
        if carry == 1:
            return ListNode(1)
        else:
            return None
    if l1 is None:
        l1 = ListNode(0, None)
    elif l2 is None:
        l2 = ListNode(0, None)
    d = l1.val + l2.val + carry
    return ListNode(d % 10, r(l1.next, l2.next, d // 10))


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        return r(l1, l2, 0)


def main():
    l1 = ListNode(9, ListNode(9, ListNode(9, ListNode(9, ListNode(9, ListNode(9, ListNode(9)))))))
    l2 = ListNode(9, ListNode(9, ListNode(9, ListNode(9, ListNode(9)))))
    s = Solution()
    s.addTwoNumbers(l1, l2)


# same


if __name__ == '__main__':
    main()
