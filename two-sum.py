# created by Muhammad Umar Farooq
# date: 14.10.2022

# link: https://leetcode.com/problems/two-sum/

from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        range_nums = range(len(nums))
        di = {num: index for (num, index) in zip(nums, range_nums)}
        for i in range_nums:
            try:
                if di[target - nums[i]] != i:
                    return [di[target - nums[i]], i]
            except Exception as e:
                pass


def main():
    s = Solution()
    print(s.twoSum([2, 7, 11, 15], 9))
    # same


if __name__ == '__main__':
    main()
